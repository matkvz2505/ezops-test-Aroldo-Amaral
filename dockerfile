FROM node:latest

WORKDIR /server

COPY . .

RUN yarn

CMD [ "yarn", "start" ]